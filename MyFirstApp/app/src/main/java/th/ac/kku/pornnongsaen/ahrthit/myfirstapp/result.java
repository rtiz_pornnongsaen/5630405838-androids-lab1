package th.ac.kku.pornnongsaen.ahrthit.myfirstapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class result extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        TextView show = (TextView) findViewById(R.id.textView);
        Intent intent = getIntent();
        show.setText(intent.getStringExtra("result"));

    }
}
