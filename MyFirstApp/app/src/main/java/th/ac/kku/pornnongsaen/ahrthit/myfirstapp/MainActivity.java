package th.ac.kku.pornnongsaen.ahrthit.myfirstapp;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import static th.ac.kku.pornnongsaen.ahrthit.myfirstapp.R.id.editText2;

public class MainActivity extends AppCompatActivity {
    EditText show;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button home = (Button) findViewById(R.id.button);
        home.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                show = (EditText) findViewById(R.id.editText2);
                String text = show.getText().toString();
                Intent go = new Intent(MainActivity.this,result.class);
                go.putExtra("result",text);
                startActivity(go);
            }
        });
    }
}
